unit async.client.mainform;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  System.Generics.Collections, Cocinasync.Async, Cocinasync.Collections,
  FMX.StdCtrls, chimera.json, FMX.Objects, FMX.Effects, FMX.Ani;

type
  TfrmMainForm = class(TForm)
    sbImages: TVertScrollBox;
    aniLoading: TAniIndicator;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FStartX : Single;
    FNextPos : TPointF;
    FTiles : TList<TRoundRect>;
    FAsync : TAsync;
    FJobs : TQueue<IJSONObject>;
    FJobCount : Integer;
    FSwapping : boolean;
    procedure StartJob;
    function APIRequest(const Path : string; const Request : IJSONObject) : IJSONObject;
    procedure RenderItem(item: IJSONObject; bmp: TBitmap);
    procedure SwapTiles(rr1, rr2 : TRoundRect);
    procedure DisableLoadingAnimation;
  public
    property Async : TAsync read FAsync;
  end;

var
  frmMainForm: TfrmMainForm;

implementation

uses Cocinasync.Jobs, System.Net.HTTPClient, System.SyncObjs;

{$R *.fmx}

type TScrollboxHack = class(TVertScrollbox);

// Generic handler that sends a json request to our AsyncServer and converts the response to json for us.
function TfrmMainForm.APIRequest(const Path : string; const Request: IJSONObject): IJSONObject;
begin
  Result := JSON();
  var http := THTTPClient.Create;
  try
    var ms := TMemoryStream.Create;
    try
      Request.SaveToStream(ms);
      ms.Position := 0;
      try
        Result.LoadFromStream(http.Post('http://127.0.0.1:8080/'+Path,ms).ContentStream);
      except
        on e: exception do
          Result.Strings['error'] := e.Message;
      end;
    finally
      ms.Free;
    end;
  finally
    http.Free;
  end;
end;

procedure TfrmMainForm.FormCreate(Sender: TObject);
begin
  FTiles := TList<TRoundRect>.Create;
  FAsync := TAsync.Create;
  FJobs := TQueue<IJSONObject>.Create(2048);

  // Allow construction of the form to complete, then begin the processing jobs.
  // This is to ensure that the form will be full size for tile layout purposes.
  Async.DoLater(
    procedure
    var
      data : IJSONObject;
    begin
      FStartX := (sbImages.Width - (Trunc(sbImages.Width / (397+10)) * (397+10))) / 2;
      FNextPos := TPointF.Create(FStartX,10);
      data := APIRequest('get_page_data', JSON());

      // queue up all of the data that we will be "rendering" in the background
      data.Arrays['venues'].Each(
        procedure(const obj : IJSONObject)
        begin
          obj.Strings['type'] := 'venue';
          FJobs.Enqueue(obj);
        end
      );
      data.Arrays['animals'].each(
        procedure(const obj : IJSONObject)
        begin
          obj.Strings['type'] := 'animal';
          FJobs.Enqueue(obj);
        end
      );

      if FJobs.Count > 0 then
      begin
        // Go ahead and start processing all the items in FJobs
        StartJob;

        // Also start a trigger that waits for jobs to complete.  When complete
        // hide the loading animation we've been spinning since program launch.
        Async.OnDo(
          function : Boolean
          begin
            Result := (FJobs.Count = 0) and (FJobCount = 0);
          end,
          procedure
          begin
            DisableLoadingAnimation;
          end, 100, 
          function : boolean
          begin
            Result := False;
          end, False, False
        );
      end else
        DisableLoadingAnimation;
        
    end, False
  );
end;

procedure TfrmMainForm.FormDestroy(Sender: TObject);
begin
  FAsync.Free;
  FTiles.Free;
end;

procedure TfrmMainForm.RenderItem(item : IJSONObject; bmp : TBitmap);
begin
  // Here we're creating everything we need to build a tile at runtime and
  // positioning it within our scrollbox.
  var rrBack := TRoundRect.Create(Self);
  rrBack.Corners := [TCorner.TopLeft, TCorner.BottomLeft];
  rrBack.Width := 397;
  rrBack.Height := 157;
  rrBack.Position.Point := FNextPos;
  rrBack.Fill.Color := $AAE2EBFF;
  rrBack.ClipChildren := True;
  FNextPos.X := FNextPos.X+rrBack.Width+20;
  if FNextPos.X+rrBack.Width > sbImages.ClientWidth-20 then
  begin
    FNextPos.X := FStartX;
    FNextPos.Y := FNextPos.Y+rrBack.Height+20;
  end;

  var rImg := TRectangle.Create(Self);
  rImg.Parent := rrBack;  
  rImg.Margins.Left := 8;
  rImg.Margins.Top := 8;
  rImg.Margins.Right := 8;
  rImg.Margins.Bottom := 8;
  rImg.Width := 137;
  rImg.Align := TAlignLayout.Right;
  rImg.Fill.Kind := TBrushKind.Bitmap;
  rImg.Fill.Bitmap.Bitmap.Assign(bmp);
  rImg.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;
  
  var lo := TLayout.Create(Self);
  lo.Parent := rrBack;
  lo.Align := TAlignLayout.Client;
  lo.Margins.Left := 30;
  lo.Margins.Top := 8;
  lo.Margins.Bottom := 8;

  var txt := TText.Create(Self);
  txt.Parent := lo;
  txt.Align := TAlignLayout.Top;
  txt.Height := 33;  
  txt.Text := item.Strings['name'];
  txt.TextSettings.Font.Size := 20;
  txt.TextSettings.Font.Style := [TFontStyle.fsBold];
  txt.TextSettings.WordWrap := False;

  txt := TText.Create(Self);
  txt.Parent := lo;
  txt.Align := TAlignLayout.Top;
  txt.Height := 24;  
  if item.Has['species'] then
    txt.Text := item.Strings['species'];
  if item.Has['capacity'] then
    txt.Text := 'Seats '+item.Integers['capacity'].ToString;
  txt.TextSettings.Font.Size := 9;
  txt.TextSettings.WordWrap := False;

  var line := TLine.Create(Self);
  line.Parent := lo;
  line.Align := TAlignLayout.Top;
  line.LineType := TLineType.Top;
  line.Height := 9;

  txt := TText.Create(Self);
  txt.Parent := lo;
  txt.Align := TAlignLayout.Client;
  txt.TextSettings.VertAlign := TTextAlign.Leading;
  txt.TextSettings.HorzAlign := TTextAlign.Leading;
  if item.Has['notes'] then
    txt.Text := item.Strings['notes'];

  var shadow := TShadowEffect.Create(Self);
  shadow.Parent := rrBack;
  var bevel := TBevelEffect.Create(Self);
  bevel.Parent := rImg;

  rrBack.Parent := sbImages;
  if rrBack.Position.Y+rrBack.Height-TScrollboxHack(sbImages).VScrollBarValue > sbImages.ClientHeight then
    sbImages.ScrollBy(0, rrBack.Height+20);

  FTiles.Add(rrBack);
end;

procedure TfrmMainForm.DisableLoadingAnimation;
begin
  //  SynchronizeIfInThread is a safe form of Synchronize call that avoids
  //  deadlocks if it happens to be called from the main thread.
  Async.SynchronizeIfInThread(
    procedure
    begin
      aniLoading.Enabled := False;
      aniLoading.Visible := False;
    end
  );

  // execute a task every x number of milliseconds
  Async.DoEvery(1000,
    function : boolean
    var
      rr1, rr2 : TRoundRect;
      begin
      Result := True; // Return True to run again after the interval passes.  False to stop the DoEvery()


      // pick two random tiles to swap
      rr1 := FTiles[Random(FTiles.Count)];
      rr2 := FTiles[Random(FTiles.Count)];
      SwapTiles(rr1, rr2);
    end
  )
end;

procedure TfrmMainForm.StartJob;
begin
  // create a job for each item in the loading queue
  FJobs.WithDo(
    procedure(item : IJSONObject)
      procedure LoadImage(const Item : IJSONObject; var Stream : TStream);
      var
        Request, Result : IJSONObject;
      begin
        Request := JSON();
        Request.Strings['type'] := Item.Strings['type'];
        if Item.Types['id'] = TJSONValueType.String then
          Request.Strings['id'] := item.Strings['id']
        else
          Request.Integers['id'] := item.Integers['id'];

        Result := APIRequest('get_image', Request);
        Stream := TBytesStream.Create(Result.Bytes['file_data']);
        Stream.Position := 0;
      end;
    var
      stImg : TStream;
    begin
      // Keep track of running jobs so that we can trigger of the completion of all jobs
      TInterlocked.Increment(FJobCount);
      try
        // The main reason for doing this in the background is perform a load
        // from the server asyncronously since loading images may be slow, especially
        // if large.
        LoadImage(item, stImg);  // creates the image stream
        try
          var bmp := TBitmap.Create;
          try
            try
              // Creation of the bitmap from the downloaded image also happens in the background
              bmp.LoadFromStream(stImg);
            except
              // don't break on image problems.
            end;

            // Synchronize the actual creation of the gui objects.  Some of this
            // may be able to be done outside of the main thread, but for simplicity
            // sake, we'll do it this way.
            Async.SynchronizeIfInThread(
              procedure
              begin
                RenderItem(item, bmp);
              end
            );
          finally
            bmp.Free;
          end;
        finally
          stImg.Free;
        end;
      finally
        TInterlocked.Decrement(FJobCount);
      end;
    end
  );
end;

procedure TfrmMainForm.SwapTiles(rr1, rr2: TRoundRect);
var
  pt1, pt2 : TPointF;
begin
  // handle a simple swap animation for two tiles.  This is used both for the
  // timed swap and the shuffle.
  if rr1 <> rr2 then
  begin
    FSwapping := True;
    try
      pt1 := rr1.Position.Point;
      pt2 := rr2.Position.Point;

      rr1.BringToFront;
      rr2.BringToFront;

      TAnimator.AnimateFloat(rr1, 'Position.Y', pt2.Y, 0.75, TAnimationType.&In, TInterpolationType.Quadratic);
      TAnimator.AnimateFloat(rr1, 'Position.X', pt2.X, 0.75, TAnimationType.&In, TInterpolationType.Cubic);

      TAnimator.AnimateFloat(rr2, 'Position.Y', pt1.Y, 0.75, TAnimationType.&In, TInterpolationType.Quartic);
      TAnimator.AnimateFloat(rr2, 'Position.X', pt1.X, 0.75, TAnimationType.&In, TInterpolationType.Sinusoidal);
    finally
      FSwapping := False;
    end;
  end;
end;

end.
