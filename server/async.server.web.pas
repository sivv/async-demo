unit async.server.web;

interface

uses
  System.SysUtils, System.Classes, Web.HTTPApp, dapi.adapter;

type
  TWeb = class(TWebModule)
    APIAdapter1: TAPIAdapter;
    procedure APIAdapter1Authenticate(Sender: TObject; Request: TWebRequest;
      var Authenticated: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WebModuleClass: TComponentClass = TWeb;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

procedure TWeb.APIAdapter1Authenticate(Sender: TObject; Request: TWebRequest;
  var Authenticated: Boolean);
begin
  Authenticated := True;
end;

end.
