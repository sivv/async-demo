unit async.server.handlers;

interface

uses System.SysUtils, System.Classes, ideal.firedac, System.SyncObjs,
  FireDAC.Phys.IB, dapi.handlers;

type
  THandler = class(TAPIHandler)
  strict private
    class var FPoolCS : TCriticalSection;
    class var FPool : TPool;
    class function GetPool: TPool; static;
  protected
    class property Pool : TPool read GetPool;
    class constructor Create;
    class destructor Destroy;
  public
  end;

implementation

{ THandler }

class constructor THandler.Create;
begin
  FPoolCS := TCriticalSection.Create;
end;

class destructor THandler.Destroy;
begin
  FPoolCS.Free;
end;

class function THandler.GetPool: TPool;
begin
  if not Assigned(FPool) then
  begin
    FPoolCS.Enter;
    try
      if not Assigned(FPool) then
      begin
        var Options := TStringList.Create;
        try
          Options.Add('Protocol=TCPIP');
          Options.Add('Server=127.0.0.1');
          Options.Add('Database=C:\Users\Public\Documents\Embarcadero\Studio\20.0\Samples\Data\dbdemos.gdb');
          Options.Add('CharacterSet=UTF8');
          Options.Add('User_Name=SYSDBA');
          Options.Add('Password=masterkey');
          FPool := NewPool('IB', Options);
          Result := FPool;
        finally
          Options.Free;
        end;
      end else
        Result := FPool;
    finally
      FPoolCS.Leave;
    end;
  end else
    Result := FPool;
end;

end.
