unit async.server.handlers.pagedata;

interface

implementation

uses System.SysUtils, System.Classes, dapi.handlers, async.server.handlers,
  ideal.firedac, chimera.json, FireDAC.Comp.Client, Data.DB, cocinasync.jobs,
  cocinasync.async;

const
  REG_GET_PAGE_DATA = '/get_page_data';

type
  TPageDataHandler = class(THandler)
  strict private
    function CollectPageData : IJSONObject;
  public
    procedure DoDispatch(Web: TAPIRequest; var Handled: Boolean); override;
  end;

{ TPageDataHandler }

function TPageDataHandler.CollectPageData: IJSONObject;
begin
  Result := JSON;

  var jVenues := TJobManager.Execute<IJSONArray>(
    function : IJSONArray
    begin
      var jsa := JSONArray();
      Pool.WithQuery(
        procedure(qry : TFDQuery)
        begin
          qry.SQL.Text := 'select VENUENO, VENUE, CAPACITY, REMARKS from VENUES';
          qry.Open;
          while not qry.Eof do
          begin
            var item := JSON();
            item.Integers['id'] := qry.FieldByName('VENUENO').AsInteger;
            item.Strings['name'] := qry.FieldByName('VENUE').AsString;
            item.Integers['capacity'] := qry.FieldByName('CAPACITY').AsInteger;
            item.Strings['notes'] := qry.FieldByName('REMARKS').AsString;
            jsa.Add(item);
            qry.Next;
          end;
        end
      );
      Result := jsa;
    end
  );

  var jBiolife := TJobManager.Execute<IJSONArray>(
    function : IJSONArray
    begin
      var jsa := JSONArray();
      Pool.WithQuery(
        procedure(qry : TFDQuery)
        begin
          qry.SQL.Text := 'select SPECIES_NO, COMMON_NAME, SPECIES_NAME, LENGTH_IN, NOTES from BIOLIFE';
          qry.Open;
          while not qry.Eof do
          begin
            var item := JSON();
            item.Integers['id'] := qry.FieldByName('SPECIES_NO').AsInteger;
            item.Strings['name'] := qry.FieldByName('COMMON_NAME').AsString;
            item.Strings['species'] := qry.FieldByName('SPECIES_NAME').AsString;
            item.Numbers['length'] := qry.FieldByName('LENGTH_IN').AsFloat;
            item.Strings['notes'] := qry.FieldByName('NOTES').AsString;
            jsa.Add(item);
            qry.Next;
          end;
        end
      );
      Result := jsa;
    end
  );

  var jAnimals := TJobManager.Execute<IJSONArray>(
    function : IJSONArray
    begin
      var jsa := JSONArray();
      Pool.WithQuery(
        procedure(qry : TFDQuery)
        begin
          qry.SQL.Text := 'select NAME, SIZ_, WEIGHT, AREA from ANIMALS';
          qry.Open;
          while not qry.Eof do
          begin
            var item := JSON();
            item.Strings['id'] := qry.FieldByName('NAME').AsString;
            item.Strings['name'] := qry.FieldByName('NAME').AsString;
            item.Strings['species'] := qry.FieldByName('AREA').AsString;
            item.Numbers['size'] := qry.FieldByName('SIZ_').AsFloat;
            item.Numbers['weight'] := qry.FieldByName('WEIGHT').AsFloat;
            jsa.Add(item);
            qry.Next;
          end;
        end
      );
      Result := jsa;
    end
  );

  Result.Arrays['venues'] := jVenues.Result;
  Result.Arrays['animals'] := jBiolife.Result;
  Result.Arrays['animals'].Merge(jAnimals.Result);
end;

procedure TPageDataHandler.DoDispatch(Web: TAPIRequest; var Handled: Boolean);
begin
  if Web.IsFor(REG_GET_PAGE_DATA) then
  begin
    Web.Send(CollectPageData);
    Handled := True;
  end;
end;

initialization
  TAPI.Register(REG_GET_PAGE_DATA, TPageDataHandler.Create);

end.
