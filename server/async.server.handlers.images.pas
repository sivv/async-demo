unit async.server.handlers.images;

interface

implementation

uses System.SysUtils, System.Classes, dapi.handlers, async.server.handlers,
  ideal.firedac, chimera.json, FireDAC.Comp.Client, Data.DB, System.Types;

const
  REG_GET_IMAGE = '/get_image';

type
  TImageHandler = class(THandler)
  private
    function LoadFileData(qry : TFDQuery) : IJSONObject;
    function RetrieveVenueImage(VenueID: integer): IJSONObject;
    function RetrieveAnimalImage(AnimalID : integer) : IJSONObject; overload;
    function RetrieveAnimalImage(const AnimalID : String) : IJSONObject; overload;
  public
    procedure DoDispatch(Web: TAPIRequest; var Handled: Boolean); override;
  end;


{ TImageHandler }

procedure TImageHandler.DoDispatch(Web: TAPIRequest; var Handled: Boolean);
begin
  if Web.IsFor(REG_GET_IMAGE) then
  begin
    // For Demo Purposes, insert random delay here
    Sleep(Random(2000));
    if Web.Msg.Strings['type'] = 'venue' then
      Web.Send(RetrieveVenueImage(Web.Msg.Integers['id']))
    else if (Web.Msg.Strings['type'] = 'animal') and (Web.Msg.Types['id'] = TJSONValueType.Number) then
      Web.Send(RetrieveAnimalImage(Web.Msg.Integers['id']))
    else if (Web.Msg.Strings['type'] = 'animal') and (Web.Msg.Types['id'] = TJSONValueType.String) then
      Web.Send(RetrieveAnimalImage(Web.Msg.Strings['id']));
    Handled := True;
  end;
end;

function TImageHandler.LoadFileData(qry: TFDQuery): IJSONObject;
begin
  Result := JSON();

  if not qry.EOF then
  begin
    var bs := TBytesStream.Create(TBlobField(qry.Fields[0]).AsBytes);
    try
      var size : DWORD;
      bs.ReadData(size, SizeOf(Size));
      bs.ReadData(size, SizeOf(Size));
      result.LoadFromStream('file_data',bs,True);
    finally
      bs.Free;
    end;
  end else
    result.AddNull('file_data');
end;

function TImageHandler.RetrieveAnimalImage(AnimalID: integer): IJSONObject;
begin
  Result := Pool.LookupValue<IJSONObject>(
    function(qry : TFDQuery) : IJSONObject
    begin
      Result := JSON();
      qry.SQL.Text := 'select GRAPHIC from BIOLIFE where SPECIES_NO='+AnimalID.ToString;
      qry.Open;
      Result := LoadFileData(qry);
    end
  );
end;

function TImageHandler.RetrieveAnimalImage(const AnimalID: String): IJSONObject;
begin
  Result := Pool.LookupValue<IJSONObject>(
    function(qry : TFDQuery) : IJSONObject
    begin
      qry.SQL.Text := 'select BMP from ANIMALS where NAME=:Name';
      qry.ParamByName('Name').AsString := AnimalID;
      qry.Open;
      Result := LoadFileData(qry);
    end
  );
end;

function TImageHandler.RetrieveVenueImage(VenueID: integer): IJSONObject;
begin
  Result := Pool.LookupValue<IJSONObject>(
    function(qry : TFDQuery) : IJSONObject
    begin
      qry.SQL.Text := 'select VENUE_MAP from VENUES where VENUENO='+VenueID.ToString;
      qry.Open;
      Result := LoadFileData(qry);
    end
  );
end;

initialization
  TAPI.Register(REG_GET_IMAGE, TImageHandler.Create);

end.
