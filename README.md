# async-demo

A Firemonkey Demo written in Delphi 10.3 Rio to showcase features of the CocinAsync library.

Also shows use of the dapi, fdpool and chimera libraries.

The Server expects Interbase to be installed anad running and the dbdemos.gdb from Delphi Rio to be installed in the default location.

## To Use

Compile both the server and the client projects

Run the server app and enter the "start" command

Run the client app on the same machine (expects localhost in the current implementation).


